import seb
import nose
import numpy as np

class TestSurfaceEnergyBalance(object):
    
    @classmethod
    def setup_class(cls):
        """this method is run once for each class before any tests are run"""
        cls.albedo = .5 #unitless
        cls.surf_emissivity = .5 #unitless
        cls.surf_rough_z0 = .02 #m
        cls.surf_zeroplane_zd = 0 #m
        cls.surf_measurem_za = 2.2 #m 
        cls.soil_temp_dmpdpth = 5 #m
        cls.soil_dmptdpth = 3 #m
        cls.soil_thermal_cond = .1296 #MJm-1d-1C-1
        cls.atmosph_emissivity = .874 #unitless
        
    @classmethod
    def teardown_class(cls):
        """this method is run once for each class __after__ all tests are run"""
        pass
    
    def setUp(self):
        """this method is run once before __each__ tests is run"""
        pass
    
    def teardown(self):
        """this method is run once after __each__ tests is run"""
        pass

    def test_net_radiation(self):
        net = seb.SurfaceEnergyBalance(self.albedo, self.surf_emissivity,
                                       self.surf_rough_z0, self.surf_zeroplane_zd,
                                       self.surf_measurem_za, self.soil_temp_dmpdpth,
                                       self.soil_dmptdpth, self.soil_thermal_cond,
                                       self.atmosph_emissivity)
        
        #Parameters, inputs, state variables
        #surf_emissivity = 0.6 #kPa
        Ldown = 25.9089329119933  # MJm-2d-1
        T_s = -0.406312 # degress Celsius
        S = 3.81492252 # MJm-2d-1
        #albedo = 0.6 # no units
        
        Net_Rad = net._net_radiation(S, T_s, Ldown)
        solution = 0.790197
        nose.tools.assert_almost_equal(solution, Net_Rad, places = 4)
        
    def test_dnet_radiation(self):
        net = seb.SurfaceEnergyBalance(self.albedo, self.surf_emissivity,
                                       self.surf_rough_z0, self.surf_zeroplane_zd,
                                       self.surf_measurem_za, self.soil_temp_dmpdpth,
                                       self.soil_dmptdpth, self.soil_thermal_cond,
                                       self.atmosph_emissivity)
        
        T_s = -0.406312 # degress Celsius
        
        DNet_Rad = net._dnet_radiation(T_s)
        solution = -0.1989432
        nose.tools.assert_almost_equal(solution, DNet_Rad, places = 4)
        
        
    def test_airVP(self):
        t = 20  ### temperature of air (celsius)
        RH = 0.9 ### relative humidity of air unitless

        solution = 0.611 * np.exp((17.3*t)/(t+237.3))*RH
        vp = seb.SurfaceEnergyBalance._VP(self, t, RH)

        nose.tools.assert_almost_equal(solution, vp)

    def test_soilVP(self):
        t = 20  ### temperature of soil (celsius)
        RH = 0.9 ### relative humidty of soil unitless

        solution = 0.611 * np.exp((17.3*t)/(t+237.3))*RH
        vp = seb.SurfaceEnergyBalance._VP(self, t, RH)

        nose.tools.assert_almost_equal(solution, vp)

    def test_init(self):
        
        surface_energy_balance = seb.SurfaceEnergyBalance(self.albedo, self.surf_emissivity,
                                                         self.surf_rough_z0, self.surf_zeroplane_zd,
                                                         self.surf_measurem_za, self.soil_temp_dmpdpth,
                                                         self.soil_dmptdpth, self.soil_thermal_cond,
                                                         self.atmosph_emissivity)
        
        nose.tools.assert_equal(surface_energy_balance.albedo, self.albedo)
        nose.tools.assert_equal(surface_energy_balance.surf_emissivity, self.surf_emissivity)
        nose.tools.assert_equal(surface_energy_balance.surf_rough_z0, self.surf_rough_z0)
        nose.tools.assert_equal(surface_energy_balance.surf_zeroplane_zd, self.surf_zeroplane_zd)
        nose.tools.assert_equal(surface_energy_balance.surf_measurem_za, self.surf_measurem_za)
        nose.tools.assert_equal(surface_energy_balance.soil_temp_dmpdpth, self.soil_temp_dmpdpth)
        nose.tools.assert_equal(surface_energy_balance.soil_dmptdpth, self.soil_dmptdpth)
        nose.tools.assert_equal(surface_energy_balance.soil_thermal_cond, self.soil_thermal_cond)
        nose.tools.assert_equal(surface_energy_balance.atmosph_emissivity, self.atmosph_emissivity)
        
        for i in range(4):
            surface_energy_balance.skin_temp.append(i+1) #make sure I created an appendable list
            
        nose.tools.assert_equal(surface_energy_balance.skin_temp, [1,2,3,4])
        
        
    def test_latent_heat(self):
        
        #  State Variables
        e_a = 0.539870  #  kPa
        e_s = 0.593137  #  kPa
        K_a = 1171.812012 #  m/d
        ob = seb.SurfaceEnergyBalance(self.albedo, self.surf_emissivity,
                                      self.surf_rough_z0, self.surf_zeroplane_zd,
                                      self.surf_measurem_za, self.soil_temp_dmpdpth,
                                      self.soil_dmptdpth, self.soil_thermal_cond,
                                      self.atmosph_emissivity)
        
        L_H = ob._latent_heat(K_a, e_a, e_s)
        expected_value =  -1.221177
        nose.tools.assert_almost_equal(expected_value, L_H, places=4)


    def test_sensible_heat(self):
        
        #  Parameters and State Variables
        K_a = 1171.812012 #  m/d
        T_a = -0.276999 #  degrees Celsius
        T_s = -0.406312 # degress Celsius

        
        SEB = seb.SurfaceEnergyBalance(self.albedo, self.surf_emissivity,
                                       self.surf_rough_z0, self.surf_zeroplane_zd,
                                       self.surf_measurem_za, self.soil_temp_dmpdpth,
                                       self.soil_dmptdpth, self.soil_thermal_cond,
                                       self.atmosph_emissivity)
        
        sens_heat = SEB._sensible_heat(K_a, T_a, T_s)
        expected_value = 0.197428
        nose.tools.assert_almost_equal(expected_value, sens_heat, places=4)

    def test_groundheat(self):

        surface_energy_balance = seb.SurfaceEnergyBalance(self.albedo, self.surf_emissivity,
                                                          self.surf_rough_z0, self.surf_zeroplane_zd,
                                                          self.surf_measurem_za, self.soil_temp_dmpdpth,
                                                          self.soil_dmptdpth, self.soil_thermal_cond,
                                                          self.atmosph_emissivity)

        # state variable
        Ts = -0.406312 # C
        nose.tools.assert_almost_equal(surface_energy_balance._groundheat(Ts), 0.233553, places=4)

    def test_K_a(self):
        von_karman = 0.4
        wind_speed = 2 #wind speed (ms-1)   
        K_a = seb.SurfaceEnergyBalance._K_a(self, wind_speed)
        import math
        expected_value = (60*60*24*(von_karman**2)*wind_speed)/((math.log((self.surf_measurem_za-self.surf_zeroplane_zd)/self.surf_rough_z0))**2)
        nose.tools.assert_almost_equal(expected_value, K_a, places=7)

    def test__solve_energy_balance(self):

        # create seb object
        surface_energy_balance = seb.SurfaceEnergyBalance(self.albedo, self.surf_emissivity,
                                                          self.surf_rough_z0, self.surf_zeroplane_zd,
                                                          self.surf_measurem_za, self.soil_temp_dmpdpth,
                                                          self.soil_dmptdpth, self.soil_thermal_cond,
                                                          self.atmosph_emissivity)
        Sdown = 3.81492252 # MJm-2d-1
        Ldown = 25.9089329119933  # MJm-2d-1
        Tair = -0.276999 #  degrees Celsius
        ws = 2 # m/s
        RHa = 0.9
        RHs = 1.0

        eb_err = surface_energy_balance._solve_energy_balance(Sdown, Ldown, Tair, ws, RHa, RHs)
        nose.tools.assert_less_equal(eb_err, 1e-6)

    def test_run_energy_balance(self):
        surface_energy_balance = seb.SurfaceEnergyBalance(self.albedo, self.surf_emissivity,
                                                          self.surf_rough_z0, self.surf_zeroplane_zd,
                                                          self.surf_measurem_za, self.soil_temp_dmpdpth,
                                                          self.soil_dmptdpth, self.soil_thermal_cond,
                                                          self.atmosph_emissivity)
        tskin, en_bal_err = surface_energy_balance.run_energy_balance('./model_data/meteo_data.tab')
        nose.tools.assert_true((en_bal_err <=1e-6).all())
